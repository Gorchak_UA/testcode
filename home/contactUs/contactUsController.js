﻿(function () {
    'use strict';

    angular
        .module('ss.home')
        .component('contactUsPage', {
            templateUrl: '/app/home/contactUs/contactUs.html',
            controller: contactUsController
        })

    contactUsController.$inject = ['errorsService', 'validationService', 'accountService', '$uibModal'];

    function contactUsController(errorsService, validationService, accountService, $uibModal) {
        var ctrl = this;

        ctrl.errors = errorsService;
        ctrl.validation = validationService;
        ctrl.progress = false;

        ctrl.contactUs = function (form) {
            if (form.$valid) {

                if (!ctrl.progress) {
                    ctrl.progress = true;
                    accountService.contactUs(ctrl.contactUsModel).then(onSuccess, onError);
                }

                function onSuccess(data) {

                    var messege = "Your email successfully sent. Please wait for the answer.";
                    var modalInstance = $uibModal.open({
                        animation: true,
                        component: 'successModal',
                        windowClass: 'share-photo-modal',
                        resolve: {
                            messege: function () {
                                return messege;
                            }
                        }
                    }).result.then(function () { }, function (res) { });

                    ctrl.contactUsModel = {};
                    ctrl.progress = false;

                    form.$setPristine();
                };

                function onError(response) {
                    ctrl.progress = false;

                    var data = response.data;

                    if (data && data.error)
                        ctrl.serverError = data.error.description;
                };
            }
        };
    }

})();
