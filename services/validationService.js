﻿(function () {
    'use strict';

    function validationService() {
        var validation = this;

        validation.email = {
            pattern: /^([A-Za-z0-9._+-]{1,60}@[A-Za-z0-9.-]{1,20}\.[A-Za-z]{2,4})$/
        };

        validation.fullName = {
            minLength: 1,
            maxLength: 120,
            pattern: /^[A-Za-z]{1}[A-Za-z0-9 _.,'()-]{0,119}$/           
        };

        validation.payPalId = {
            pattern: /^([A-Za-z0-9._+-]{1,60}@[A-Za-z0-9.-]{1,20}\.[A-Za-z]{2,4})$/
        };

        validation.password = {
            minLength: 6,    
            maxLength: 40,
            pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/
        };

        validation.messege = {
            maxLength: 300,
            minLength: 20
        };
    };

    validationService.$inject = [];

    angular
        .module('validation.service', [])
        .service('validationService', validationService);

})();