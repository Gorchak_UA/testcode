﻿(function () {
    angular.module('ss.home', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('home', {
                    url: "/",
                    template: '<home-page></home-page>',
                    data: {
                        pageTitle: 'Home',
                        noLogin: true
                    },
                    resolve: {
                        authorize: ['authorizationService', function (authorizationService) {
                            return authorizationService.authorize();
                        }]
                    }
                })
                .state('contactUs', {
                    url: "/contactus",
                    template: '<contact-us-page></contact-us-page>',
                    data: {
                        pageTitle: 'Contact Us',
                        noLogin: true
                    }
                })
                .state('faq', {
                    url: "/faq",
                    template: '<faq-page></faq-page>',
                    data: {
                        pageTitle: 'FAQ',
                        noLogin: true
                    }
                })
        }])
})();