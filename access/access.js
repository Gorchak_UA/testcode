﻿(function () {
    angular.module('ss.access', [])
        .constant('accessConfig', {
            notAuthState: 'signin',
            authState: 'profile',
            accessdeniedState: 'accessdenied'
        })
        .run(['$rootScope', '$state', 'currentUserService', 'accessConfig', run]);

    function run($rootScope, $state, currentUserService, accessConfig) {
        $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
            var noLogin = false;

            if (toState.data != null)
                if (toState.data.noLogin)
                    noLogin = toState.data.noLogin;

            if (noLogin)
                return; // no need to redirect 

            var authenticated = currentUserService.isAuthenticated();

            // if user not auth redirect to state from accessConfig
            if (authenticated === false) {
                e.preventDefault(); // stop current execution
                $state.go(accessConfig.notAuthState, {}, { reload: true });
            } // get role from state data and check access
            else if (toState.data && toState.data.accessRole) {
                var userRole = currentUserService.isInRole(toState.data.accessRole);

                if (!userRole) {
                    e.preventDefault(); // stop current execution
                    $state.go(accessConfig.accessdeniedState, {}, { reload: true });
                }
            }
        });
    }
})();