﻿(function () {
    'use strict';

    angular.module('ss.account')
        .component('signupPage', {
            templateUrl: '/app/account/views/signup.html',
            controller: signupCoachController
        });

    signupCoachController.$inject = ['$state', 'accountService', 'accessConfig', 'errorsService', 'validationService','$window'];

    function signupCoachController($state, accountService, accessConfig, errorsService, validationService, $window) {
        var ctrl = this;
        ctrl.serverError;
        ctrl.widgetId = null;
        ctrl.showLoader = false;

        ctrl.errors = errorsService;
        ctrl.validation = validationService;
        ctrl.progress = false;


        ctrl.signUp = function (form) {
            if (form.$valid && ctrl.confirmPassword == ctrl.register.password && !ctrl.isSpace(ctrl.confirmPassword)) {

                if (!ctrl.progress) {
                    ctrl.progress = true;
                    accountService.signUp(ctrl.register).then(onSuccess, onError);
                }
               
                function onSuccess (data) {
                    ctrl.progress = false;

                    ctrl.signInInfo = {
                        grant_type: 'password',
                        username: ctrl.register.email,
                        password: ctrl.register.password
                    };

                    accountService.signIn(ctrl.signInInfo).then(function onSuccess() {
                        $state.go(accessConfig.authState, {}, { reload: true });
                        $window.location.reload();
                    });
                };

                function onError(response) {
                    ctrl.progress = false;

                    var data = response.data;

                    if (data && data.error)
                        ctrl.serverError = data.error.description;
                };
            }
        };

        ctrl.isMatch = function () {
            if (ctrl.register) {
                if (ctrl.confirmPassword != ctrl.register.password) {
                    return true;
                }
                else {
                    return false;
                }
            }
        };

        ctrl.isSpace = function (password) {
            if (ctrl.register && password) {
                var space = "";
                space = angular.copy(password);
                space = space.replace(/\s/g, "");

                if (space.length == 0) {
                    return true;
                }
                else {
                    return false;
                }
            }
            return false;
        };

    }

})();
