﻿(function () {
    'use strict';

    paymentController.$inject = ['$state', '$stateParams', 'paymentService'];

    function paymentController($state, $stateParams, paymentService) {
        var ctrl = this;

        ctrl.photoSetId = $stateParams.id;

        ctrl.paymentService = paymentService;
    }

    angular.module('app')
        .component('paymentPage', {
            templateUrl: '/app/payment/payment.html',
            controller: paymentController
        })
})();
