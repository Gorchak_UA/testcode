﻿(function () {
    'use strict';

    photoBoughtController.$inject = ['$state', 'photoSetsService', '$timeout', 'photoService', '$stateParams', 'currentUserService'];

    function photoBoughtController($state, photoSetsService, $timeout, photoService, $stateParams, currentUserService) {
        var ctrl = this;

        var skip = 0;

        ctrl.$onInit = function () {
            ctrl.progress = false;
            ctrl.photoSetId = $stateParams.id;
            ctrl.currentUserService = currentUserService;
            ctrl.photoService = photoService;

            ctrl.firstPhotosArray = [];
            ctrl.secondPhotosArray = [];
            ctrl.thirdPhotosArray = [];

                ctrl.photoIdnex = "originalPhoto";
        };

        ctrl.getMorePhotos = function () {
            if (!ctrl.progress && !ctrl.noMore) {
                ctrl.progress = true;

                photoService.getBoughtPhotos(skip, ctrl.photoSetId).then(onSuccess, onError);

                function onSuccess(response) {

                    skip = skip + response.length;
                    ctrl.progress = false;

                    if (response && response.length === 0) {
                        ctrl.noMore = true;
                    }

                    ctrl.photoService.setPhotosToArrays(response, ctrl.firstPhotosArray, ctrl.secondPhotosArray, ctrl.thirdPhotosArray);
                    delete ctrl.serverError;
                };

                function onError(response) {
                    ctrl.progress = false;
                    var data = response.data;
                    if (data && data.error)
                        ctrl.serverError = data.error.description;
                };
            }
        };
    }

    angular.module('app')
        .component('photoBoughtPage', {
            templateUrl: '/app/photoBought/photoBought.html',
            controller: photoBoughtController
        })
})();
