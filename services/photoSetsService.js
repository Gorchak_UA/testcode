﻿(function () {
    'use strict';

    function photoSetsService($http, $rootScope, $state, localStorageService, toastr, currentUserService, utilService) {
        var factory = this;
        var api = "/api/v1";

        factory.me = function (id) {
            return $http.get(api + "/profile/me", { params: { id: id } })
                .then(utilService.successCallback())
                .catch(utilService.errorCallbackToastr());
        }

        factory.get = function (id) {
            return $http.get(api + "/profile/" + id)
                .then(utilService.successCallback())
                .catch(utilService.errorCallbackToastr());
        }

        factory.isInRole = function (roles, role) {
            if (!roles)
                return false;

            return roles.some(function (item) {
                return role.toLowerCase() === item.toLowerCase();
            });
        }

        factory.createPhotoset = function (model) {
            return $http.post('/api/v1/photoSet/createPhotoset', model)
                .then(utilService.successCallback());
        };

        factory.getPhotoSets = function (skip) {
            return $http.get('/api/v1/photoSet/getPhotoSets', { params: { skip: skip } })
                .then(utilService.successCallback())
                .catch(utilService.errorCallbackToastr());
        };

        factory.deletePhotoSet = function (photoSetId) {
            return $http.delete('/api/v1/photoSet/deletePhotoSet', { params: { photoSetId: photoSetId } })
                .then(utilService.successCallback("Successfully deleted."))
                .catch(utilService.errorCallbackToastr());
        };

        factory.sharePhotos = function (photoSetId) {
            return $http.post('/api/v1/photoSet/sharePhotos/' + photoSetId)
                .then(utilService.successCallback());
        };

        factory.getPhotoSet = function (photoSetId) {
            return $http.get('/api/v1/photoSet/getPhotoSet', { params: { photoSetId: photoSetId } })
                .then(utilService.successCallback())
                .catch(utilService.errorCallbackToastr());
        };

        factory.getPhotoSetDateCreate = function (photoSetId) {
            return $http.get('/api/v1/photoSet/getPhotoSetDateCreate', { params: { photoSetId: photoSetId } })
                .then(utilService.successCallback())
                .catch(utilService.errorCallbackToastr());
        };
    };

    photoSetsService.$inject = ['$http', '$rootScope', '$state', 'localStorageService', 'toastr', 'currentUserService', 'utilService'];

    angular
        .module('photosets.service', [])
        .service('photoSetsService', photoSetsService);

})();