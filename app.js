﻿/* App Module */
angular.module('app', [
    'ui.router',
    'ui.bootstrap',
    'mp.autoFocus',
    'ss.error',
    'ss.access',
    'ss.loading',
    'ss.footer',
    'ss.home',
    'ss.account',
    'ss.photosets',
    'ss.photoset',
    'ss.admin',
    'ss.photobought',
    'ss.payment',
    'ss.termsOfUse',
    'ss.privacyPolicy',
    'ss.settings',
    'ss.photo',
    'account.service',
    'authorization.service',
    'currentUser.service',
    'photosets.service',
    'photo.service',
    'adminPayment.service',
    'settings.service',
    'realtime.service',
    'util.service',
    'errors.service',
    'validation.service',
    'profile.service',
    'payment.service',
    'ngMessages',
    'interceptorRequestError',
    'toastr',
    'LocalStorageModule',
    'ngFileUpload',
    'ngFileSaver',
    'infinite-scroll'
])
    .constant('appConfig', {
        'urlAzureBlob': 'https://test.blob.core.windows.net/'
    })
    .config(['$sceDelegateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 'toastrConfig', '$stateProvider', 'accessConfig', 'errorConfig', '$qProvider',
        function ($sceDelegateProvider, $urlRouterProvider, $locationProvider, $httpProvider, toastrConfig, $stateProvider, accessConfig, errorConfig, $qProvider) {

            angular.extend(toastrConfig, {
                allowHtml: true,
                newestOnTop: true,
                progressBar: true,
                preventOpenDuplicates: true
            });

            angular.extend(accessConfig, {
                notAuthState: 'signin',
                authState: 'photoSets',
                accessdeniedState: 'accessdenied',
                adminState: 'admin'
            });

            errorConfig.exeptUrlToastr.push('/api/v1/account/register');
            errorConfig.exeptUrlToastr.push('/api/v1/profile/withdrawFunds');
            errorConfig.exeptUrlToastr.push('/api/v1/settings/ChangePassword');

            $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });

            $urlRouterProvider.otherwise("/error/not-found");

            $sceDelegateProvider.resourceUrlWhitelist([
                'self'
            ]);
        }])
    .run(['$http', '$rootScope', '$state', 'localStorageService', 'currentUserService', function ($http, $rootScope, $state, localStorageService, currentUserService) {
        //realtimeService.on("testClient", function (data) {
        //    console.log(data);
        //    debugger;
        //});

        //realtimeService.invoke("Send", "Send 111111");

        $rootScope.$state = $state;

        var userInfo = localStorageService.get('currentUser');

        if (userInfo && new Date(userInfo.expires) > new Date()) {
            angular.extend(currentUserService, userInfo);
            $http.defaults.headers.common.Authorization = userInfo.token;
            //console.log("currentUser", userInfo);
        }
    }]);
