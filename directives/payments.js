﻿(function () {

    function payments($timeout, paymentService, $state, $stateParams, $uibModal) {
        return {
            restrict: 'A',
            scope: true,
            link: function (scope, element, attrs) {

                var form = element.find('#CreditCardForm');

                scope.disableBtnCreditCard = true;
                scope.progressPayments = false;
                scope.paymentFormValidate = false;
                scope.paymentFormSubmit = false;


                paymentService.getToken().then(function () {
                    // Create a client.
                    braintree.client.create({
                        authorization: paymentService.token
                    },
                        function (clientErr, clientInstance) {
                            authorizationSuccessful(clientErr, clientInstance);
                        });
                });

                function authorizationSuccessful(clientErr, clientInstance) {

                    // Stop if there was a problem creating the client.
                    // This could happen if there is a network error or if the authorization
                    // is invalid.
                    if (clientErr) {
                        return;
                    }

                    // Create a PayPal component.
                    braintree.paypal.create({
                        client: clientInstance
                    },
                        function (paypalErr, paypalInstance) {
                            payPalBinder(paypalErr, paypalInstance);
                        }
                    );

                    // Create input fields and add text styles
                    braintree.hostedFields.create({
                        client: clientInstance,
                        styles: {
                            'input': {
                                'color': '#888',
                                'font-size': '16px',
                                'transition': 'color 0.1s',
                                'line-height': '3'
                            },
                            // Style the text of an invalid input
                            'input.invalid': {
                                'color': '#E53A40'
                            },
                            // placeholder styles need to be individually adjusted
                            '::-webkit-input-placeholder': {
                                'color': 'rgba(0,0,0,0.6)'
                            },
                            ':-moz-placeholder': {
                                'color': 'rgba(0,0,0,0.6)'
                            },
                            '::-moz-placeholder': {
                                'color': 'rgba(0,0,0,0.6)'
                            },
                            ':-ms-input-placeholder': {
                                'color': 'rgba(0,0,0,0.6)'
                            }

                        },
                        // Add information for individual fields
                        fields: {
                            number: {
                                selector: '#card-number',
                                placeholder: 'Card Number'
                            },
                            cvv: {
                                selector: '#cvv',
                                placeholder: 'CVV'
                            },
                            expirationDate: {
                                selector: '#expiration-date',
                                placeholder: 'Expiry Date'
                            }
                        }
                    }, function (err, hostedFieldsInstance) {
                        creditCardBinder(err, hostedFieldsInstance);
                    });

                    function creditCardBinder(err, hostedFieldsInstance) {
                        if (err) {
                            return;
                        }

                        hostedFieldsInstance.on('validityChange', function (event) {
                            // Check if all fields are valid, then show submit button
                            var formValid = Object.keys(event.fields).every(function (key) {
                                return event.fields[key].isValid;
                            });

                            scope.$apply(function () {

                                if (formValid) {
                                    scope.disableBtnCreditCard = false;
                                } else {
                                    scope.disableBtnCreditCard = true;
                                }
                            });

                        });

                        hostedFieldsInstance.on('empty', function (event) {
                            $('#card-image').removeClass();
                            $(form).removeClass();
                        });

                        hostedFieldsInstance.on('cardTypeChange', function (event) {
                            // Change card bg depending on card type
                            if (event.cards.length === 1) {
                                $(form).removeClass().addClass(event.cards[0].type);
                                $('#card-image').removeClass().addClass(event.cards[0].type);

                                // Change the CVV length for AmericanExpress cards
                                if (event.cards[0].code.size === 4) {
                                    hostedFieldsInstance.setPlaceholder('cvv', '1234');
                                }
                            } else {
                                hostedFieldsInstance.setPlaceholder('cvv', '123');
                            }
                        });


                        element.find("#CreditCardButton").on('click', function (event) { creditCardTokenize(hostedFieldsInstance, event); });
                    };

                    function creditCardTokenize(hostedFieldsInstance, event) {
                        scope.paymentFormSubmit = true;

                        event.preventDefault();
                        scope.$apply(function () {
                            scope.progressPayments = true;
                            //scope.paymentFormValidate = true;
                        });

                        hostedFieldsInstance.tokenize(function (err, payload) {
                            scope.paymentFormValidate = false;

                            if (err) {
                                scope.$apply(function () {
                                    scope.progressPayments = false;
                                    scope.serverError = err.message;
                                });
                                return;
                            }

                            delete scope.serverError;
                            createPurchase(payload.nonce);
                        });
                    };

                    function payPalBinder(paypalErr, paypalInstance) {

                        // Stop if there was a problem creating PayPal.
                        // This could happen if there was a network error or if it's incorrectly
                        // configured.
                        if (paypalErr) {
                            return;
                        }
                        // Enable the button.
                        //  paypalButton.removeAttribute('disabled');

                        element.find("#PaypalButton").on('click', function () { payPalTokenize(paypalInstance); });

                    }
                };


                function payPalTokenize(paypalInstance) {
                    scope.$apply(function () {
                        scope.progressPayments = true;
                    });
                    paypalInstance.tokenize({
                        flow: 'vault'
                    },
                        function (tokenizeErr, payload) {

                            // Stop if there was an error.
                            if (tokenizeErr) {
                                if (tokenizeErr.type !== 'CUSTOMER') {
                                }

                                scope.$apply(function () {
                                    scope.progressPayments = false;
                                    scope.serverError = err.message;
                                });

                                return;
                            }

                            delete scope.serverError;
                            createPurchase(payload.nonce);

                        });
                };

                function createPurchase(nonce) {
                    paymentService.buyPhotos(nonce).then(onSuccess, onError);

                    function onSuccess(data) {
                        $state.go('photoset', { id: $stateParams.id });
                    };

                    function onError(response) {
                        
                        var data = response.data;

                        var messege = data.error.description;

                        var modalInstance = $uibModal.open({
                            animation: true,
                            component: 'errorModal',
                            windowClass: 'share-photo-modal',
                            resolve: {
                                messege: function () {
                                    return messege;
                                }
                            }
                        }).result.then(function () { }, function (res) { });

                        scope.progressPayments = false;
                    };
                }
            }
        }
    }

    payments.$inject = ['$timeout', 'paymentService', '$state', '$stateParams', '$uibModal'];

    angular
        .module('app')
        .directive('payments', payments);

})();