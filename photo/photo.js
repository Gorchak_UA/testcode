﻿(function () {
    angular.module('ss.photo', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('photo', {
                    url: "/photo/:id",
                    template: '<photo-page photo="$resolve.photo"></photo-page>',
                    params: {
                        userId: ['currentUserService', function (currentUserService) {
                            return currentUserService.id
                        }],
                    },
                    data: {
                        pageTitle: 'Original Photo',
                        noLogin: true
                    },
                    resolve: {
                        photo: ['$stateParams', 'photoService', function ($stateParams, photoService) {
                            if ($stateParams.userId)
                                return photoService.getPhoto($stateParams.id);
                            else
                                return photoService.getClientPhoto($stateParams.id);
                        }]
                    }
                })
        }])
})();