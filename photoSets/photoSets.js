﻿(function () {
    angular.module('ss.photosets', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('photoSets', {
                    url: "/photosets/:id",
                    template: '<photo-sets-page profile="$resolve.profile" photo-sets="$resolve.photosets"></photo-sets-page>',
                    params: {
                        id: ['currentUserService', function (currentUserService) {
                            return currentUserService.id
                        }],
                    },
                    data: {
                        pageTitle: 'Photo Sets'
                    },
                    resolve: {
                        profile: ['$stateParams', 'photoSetsService', 'currentUserService', function ($stateParams, photoSetsService, currentUserService) {
                            if (currentUserService.id === $stateParams.id)
                                return photoSetsService.me();
                            else
                                return photoSetsService.get($stateParams.id);
                        }],
                        photosets: ['photoSetsService', function (photoSetsService) {
                            return photoSetsService.getPhotoSets(0);
                        }]
                    }
                })
        }])
})();