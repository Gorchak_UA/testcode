﻿(function () {
    'use strict';

    angular
        .module('payment.service', [])
        .service('paymentService', paymentService);

    paymentService.$inject = ['$http', 'utilService', '$q', 'FileSaver'];

    function paymentService($http, utilService, $q, FileSaver) {
        var factory = this;
        var api = "/api/v1";

        factory.selectedPhotos = [];

        factory.checkout = function (selectedPhotos) {
            factory.selectedPhotos = selectedPhotos;
        };

       //------------------------payment-----------------------------//

        factory.token = null;

        factory.getToken = function () {

            var deferred = $q.defer();

            if (factory.token) {
                deferred.resolve();
            } else {
                $http.get("/api/v1/payment/GetToken").then(function (result) {
                    factory.token = result.data;
                    deferred.resolve();
                });
            }

            return deferred.promise;
        }

      
        //---------------------------------------------------------------//

        factory.buyPhotos = function (nonce) {
            var model = {};
            model.nonce = nonce;
            model.photosIds = factory.selectedPhotos;

            
            return $http.post('/api/v1/payment/buyPhotos', model)
                .then(function (response) {
                    var deferred = $q.defer();
                    factory.downloadPhotos().then(function () {
                        deferred.resolve(response);
                    });
                    factory.selectedPhotos = [];
                    return deferred.promise;
                })
                .catch(utilService.errorCallbackToastr());
        };

        factory.downloadPhotos = function () {
            var model = {};
            model.photosIds = factory.selectedPhotos;

            return $http({
                url: api + "/photo/downloadPhotos",
                method: 'POST',
                data: model,
                responseType: 'arraybuffer'  
            })
                .then(onSuccess)
                .catch(utilService.errorCallbackToastr());

            function onSuccess(data) {
                var blob = new File([data.data], { type: 'application/zip' });
                FileSaver.saveAs(blob, "photos.zip");
                factory.selectedPhotos = [];
            };
        };

        return factory;
    };   
})();