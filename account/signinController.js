﻿(function () {
    'use strict';

    angular.module('ss.account')
        .component('signinPage', {
            templateUrl: '/app/account/views/signin.html',
            controller: signinController
        })
        .component('signinAdminPage', {
            templateUrl: '/app/admin/signIn/adminSignIn.html',
            controller: signinController
        });

    signinController.$inject = ['$state', 'accountService', 'accessConfig', 'currentUserService', 'errorsService', 'validationService','$window'];

    function signinController($state, accountService, accessConfig, currentUserService, errorsService, validationService, $window) {
        var ctrl = this;

        ctrl.errors = errorsService;
        ctrl.validation = validationService;
        ctrl.progress = false;

        ctrl.signInInfo = {
            grant_type: 'password',
            username: null,
            password: null
        };

        ctrl.signIn = function (form) {
            if (form.$valid) {

                if (!ctrl.progress) {
                    ctrl.progress = true;
                    accountService.signIn(ctrl.signInInfo).then(onSuccess, onError);
                }
                

                function onSuccess(response) {
                    ctrl.progress = false;
                    var data = response.data;

                    if (currentUserService.isAdmin()) {
                        $state.go(accessConfig.adminState, {}, { reload: true });
                    } else {
                        $state.go(accessConfig.authState, {}, { reload: true });                      
                    }
                    $window.location.reload();
                };

                function onError(response) {
                    ctrl.progress = false;
                    var data = response.data;

                    if (data.error_description)
                        ctrl.serverError = data.error_description;
                };
            }
        };
    }

})();
