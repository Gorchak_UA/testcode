﻿(function () {
    angular.module('ss.photoset', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('photoset', {
                    url: "/photoset/:id",
                    template: '<photo-set-page></photo-set-page>',
                    data: {
                        pageTitle: 'Photo Set',
                        noLogin: true
                    }
                })
        }])
})();