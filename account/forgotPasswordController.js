﻿(function () {
    'use strict';

    function forgotPasswordController(accountService, errorsService, validationService) {
        var ctrl = this;

        ctrl.emailSent = false;
        ctrl.errors = errorsService;
        ctrl.validation = validationService;
        ctrl.progress = false;

        ctrl.forgotPassword = function (form) {
            if (form.$valid) {

                if (!ctrl.progress) {
                    ctrl.progress = true;

                    accountService.forgotPassword(ctrl.model).then(onSuccess, onError);
                }
                
                function onSuccess(data) {
                    ctrl.emailSent = true;
                    ctrl.model = null;
                    ctrl.progress = false;
                }
                function onError(response) {
                    ctrl.progress = false;

                    var data = response.data;

                    if (data.modelState && data.modelState.identity) {
                        if (data.modelState.identity[1])
                            ctrl.serverError = data.modelState.identity[1];
                        else
                            ctrl.serverError = data.modelState.identity[0];
                    }
                }
            }
        }

    }

    forgotPasswordController.$inject = ['accountService','errorsService', 'validationService'];

    angular.module('ss.account')
        .component('forgotPasswordPage', {
            templateUrl: '/app/account/views/forgotPassword.html',
            controller: forgotPasswordController
        });
})()
