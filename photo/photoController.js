﻿(function () {
    'use strict';

    photoController.$inject = ['$state', 'currentUserService', 'photoService', 'paymentService', '$uibModal'];

    function photoController($state, currentUserService, photoService, paymentService, $uibModal) {
        var ctrl = this;       

        ctrl.$onInit = function () {
            ctrl.progress = false;
            ctrl.photoService = photoService;
            ctrl.currentUserService = currentUserService;
            ctrl.paymentService = paymentService;

            if (currentUserService.isAuthenticated()) {
                ctrl.photoIdnex = "originalPhoto";
            }
            else {
                ctrl.photoIdnex = "phobroPhoto";
            }
        };

        ctrl.selectPhotos = function (event, photoId) {
            if (ctrl.paymentService.selectedPhotos.indexOf(photoId) == -1) {
                event.stopPropagation();
                ctrl.paymentService.selectedPhotos.push(photoId);
                ctrl.paymentService.isSelectPhoto = true;
            }
            else {
                ctrl.paymentService.selectedPhotos.splice(ctrl.paymentService.selectedPhotos.indexOf(photoId), 1);
                event.stopPropagation();
            }        
        };

        ctrl.checkout = function () {
            paymentService.checkout(ctrl.paymentService.selectedPhotos);
            $state.go('payment', { id: ctrl.photo.photoSetId });
        };

        ctrl.deletePhoto = function (photoId) {
            if (!ctrl.progress) {
                ctrl.progress = true;

                var isAccept = false;
                var messege = "Are you sure you want to delete this photo?";
                var modalInstance = $uibModal.open({
                    animation: true,
                    component: 'acceptModal',
                    windowClass: 'accept-photo-modal',
                    resolve: {
                        isAccept: function () {
                            return isAccept;
                        },
                        messege: function () {
                            return messege;
                        }
                    }
                }).result.then(function (isAccept) {
                    if (isAccept) {
                        photoService.deletePhoto(photoId, ctrl.photo.photoSetId).then(onSuccess, onError);
                    }
                    else {
                        ctrl.progress = false;
                    }
                }, function (res) { });
            }

            function onSuccess(data) {
                ctrl.progress = false;
                $state.go('photoset', { id: ctrl.photo.photoSetId });
                delete ctrl.serverError;
            };

            function onError(response) {
                ctrl.progress = false;
                var data = response.data;
                if (data && data.error)
                    ctrl.serverError = data.error.description;
            };
        };
    }

    angular.module('app')
        .component('photoPage', {
            templateUrl: '/app/photo/photo.html',
            controller: photoController,
            bindings: {
                photo: '<'
            }
        })
})();
