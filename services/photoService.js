﻿(function () {
    'use strict';

    angular
        .module('photo.service', [])
        .service('photoService', photoService);

    photoService.$inject = ['$http', '$rootScope', '$state', 'localStorageService', 'toastr', 'currentUserService', 'utilService', 'Upload'];

    function photoService($http, $rootScope, $state, localStorageService, toastr, currentUserService, utilService, Upload) {
        var factory = this;
        var api = "/api/v1";

        factory.uploadPhotos = function (files, photoSetId) {

            return Upload.upload({
                url: "/api/v1/photo/uploadPhotos/" + photoSetId,
                method: "POST",
                data: {
                    files: files
                }

            }).then(utilService.successCallback());
        };

        factory.getPhotos = function (skip, photoSetId) {
            return $http.get('/api/v1/photo/getPhotos', { params: { skip: skip, photoSetId: photoSetId } })
                .then(utilService.successCallback())
                .catch(utilService.errorCallbackToastr());
        };

        factory.getClientPhotos = function (skip, photoSetId) {
            return $http.get('/api/v1/photo/getClientPhotos', { params: { skip: skip, photoSetId: photoSetId } })
                .then(utilService.successCallback())
                .catch(utilService.errorCallbackToastr());
        };

        factory.getPhoto = function (photoId) {
            return $http.get('/api/v1/photo/getPhoto', { params: { photoId: photoId } })
                .then(utilService.successCallback())
                .catch(utilService.errorCallbackToastr());
        };

        factory.getClientPhoto = function (photoId) {
            return $http.get('/api/v1/photo/getClientPhoto', { params: { photoId: photoId } })
                .then(utilService.successCallback())
                .catch(utilService.errorCallbackToastr());
        };

        factory.deletePhoto = function (photoId, photoSetId) {
            return $http.delete('/api/v1/photo/deletePhoto', { params: { photoId: photoId, photoSetId: photoSetId } })
                .then(utilService.successCallback());
        };

        factory.getBoughtPhotos = function (skip, photoSetId) {
            return $http.get('/api/v1/photo/getBoughtPhotos', { params: { skip: skip, photoSetId: photoSetId } })
                .then(utilService.successCallback())
                .catch(utilService.errorCallbackToastr());
        };

        factory.setPhotosToArrays = function (photosArray, firstPhotosArray, secondPhotosArray, thirdPhotosArray) {

            var count = photosArray.length / 3;
            var intCount = Math.ceil(count);
            var firstIndex = 0;
            var secondIndex = 1;
            var thirdIndex = 2;

            for (var i = 0; i < intCount; i++) {

                if (photosArray[firstIndex] && firstPhotosArray.length == secondPhotosArray.length && thirdPhotosArray.length == secondPhotosArray.length) {
                    firstPhotosArray.push(photosArray[firstIndex]);
                }

                if (photosArray[secondIndex] && firstPhotosArray.length > secondPhotosArray.length) {
                    secondPhotosArray.push(photosArray[secondIndex]);
                }

                if (photosArray[thirdIndex] && firstPhotosArray.length == secondPhotosArray.length && thirdPhotosArray.length < secondPhotosArray.length) {
                    thirdPhotosArray.push(photosArray[thirdIndex]);
                }

                firstIndex = firstIndex + 3;
                secondIndex = secondIndex + 3;
                thirdIndex = thirdIndex + 3;
            }
        };            
    };
})();