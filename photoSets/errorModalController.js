﻿(function () {
    'use strict';

    errorModalController.$inject = ['$state', 'accountService','$timeout'];

    function errorModalController($state, accountService, $timeout) {
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.messege = ctrl.resolve.messege;
            $timeout(function () {
                ctrl.close();
            }, 5000);       
        }

    }

    angular.module('app')
        .component('errorModal', {
            templateUrl: '/app/photoSets/views/errorModal.html',
            controller: errorModalController,
            bindings: {
                resolve: '=',
                close: '&',
                dismiss: '&'
            }
        })
})();
