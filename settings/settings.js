﻿(function () {
    angular.module('ss.settings', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('settings', {
                    url: "/settings",
                    template: '<settings-page user="$resolve.user"></settings-page>',
                    data: {
                        pageTitle: 'Settings'
                    },
                    resolve: {
                        user: ['settingsService', function (settingsService) {
                            return settingsService.getUserSettings();
                        }]
                    }
                })
        }])
})();