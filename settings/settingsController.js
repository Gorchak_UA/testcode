﻿(function () {
    'use strict';

    settingsController.$inject = ['$state', 'accountService', 'errorsService', 'validationService', 'settingsService', '$uibModal'];

    function settingsController($state, accountService, errorsService, validationService, settingsService, $uibModal) {
        var ctrl = this;

        ctrl.errors = errorsService;
        ctrl.validation = validationService;
        ctrl.progress = false;
        ctrl.logOut = function () {
            accountService.logOut();
        };

        ctrl.$onInit = function () {
            ctrl.updateProfileModel = {
                fullName: ctrl.user.fullName,
                email: ctrl.user.email,
                payPalId: ctrl.user.payPalId
            };
        };
        
        ctrl.updateProfile = function (form)
        {
            if (form.$valid && (ctrl.updateProfileModel.fullName !== ctrl.user.fullName || ctrl.updateProfileModel.email !== ctrl.user.email || ctrl.updateProfileModel.payPalId !== ctrl.user.payPalId)) {
                if (!ctrl.progress) {

                    ctrl.progress = true;

                    settingsService.updateProfile(ctrl.updateProfileModel)
                        .then(onSuccess, onError);
                }

                function onSuccess(data) {

                    var messege = "Your profile was updated successfully.";
                    var modalInstance = $uibModal.open({
                        animation: true,
                        component: 'successModal',
                        windowClass: 'share-photo-modal',
                        resolve: {
                            messege: function () {
                                return messege;
                            }
                        }
                    })
                        .result.then(function () { }, function (res) { });

                    ctrl.user.fullName = ctrl.updateProfileModel.fullName;
                    ctrl.user.email = ctrl.updateProfileModel.email;
                    ctrl.user.payPalId = ctrl.updateProfileModel.payPalId;

                    ctrl.progress = false;
                    delete ctrl.profileServerError;
                };

                function onError(response) {
                    ctrl.progress = false;
                    var data = response.data;
                    if (data && data.error)
                        ctrl.profileServerError = data.error.description;
                };
            }
            else
            {
                ctrl.profileServerError = "User not modified.";
            }
        }

        ctrl.updatePassword = function (form) {           

            if (form.$valid && (ctrl.updatePasswordModel.confirmPassword == ctrl.updatePasswordModel.newPassword && !ctrl.isSpace(ctrl.updatePasswordModel.confirmPassword) && !ctrl.isSpace(ctrl.updatePasswordModel.newPassword))) {

                if (!ctrl.progress) {

                    ctrl.progress = true;

                    accountService.changePassword(ctrl.updatePasswordModel)
                        .then(onSuccess, onError);

                    function onSuccess(data) {
                        ctrl.progress = false;
                        delete ctrl.serverError;
                    };

                    function onError(response) {
                        var data = response.data;
                        if (data && data.error)
                            ctrl.serverError = data.error.description;
                        ctrl.progress = false;
                    };

                }
            }
        };

        ctrl.isMatch = function () {
            if (ctrl.updatePasswordModel) {
                if (ctrl.updatePasswordModel.confirmPassword != ctrl.updatePasswordModel.newPassword) {
                    return true;
                }
                else {
                    return false;
                }
            }           
        };

        ctrl.isSpace = function (password) {          
            if (ctrl.updatePasswordModel && password) {  
                var space = "";
                space = angular.copy(password);
                space = space.replace(/\s/g, "");

                if (space.length == 0) {
                    return true;
                }
                else {
                    return false;
                }
            }
            return false;
        };

    };

    angular.module('app')
        .component('settingsPage', {
            templateUrl: '/app/settings/settings.html',
            controller: settingsController,
            bindings: {
                user: '<'
            }
        })
})();
