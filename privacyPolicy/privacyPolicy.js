﻿(function () {
    angular.module('ss.privacyPolicy', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('privacyPolicy', {
                    url: "/privacypolicy",
                    template: '<privacy-policy-page></privacy-policy-page>',
                    data: {
                        pageTitle: 'Privacy Policy',
                        noLogin: true
                    }
                })
        }])
})();