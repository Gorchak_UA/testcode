﻿(function () {
    'use strict';

    photoSetController.$inject = ['$state', 'photoSetsService', '$timeout', 'photoService', '$stateParams', 'currentUserService', 'paymentService', 'FileSaver', 'Upload','$uibModal'];

    function photoSetController($state, photoSetsService, $timeout, photoService, $stateParams, currentUserService, paymentService, FileSaver, Upload, $uibModal) {
        var ctrl = this;

        var skip = 0;

        ctrl.$onInit = function () {
            ctrl.progress = false;
            ctrl.photoSetId = $stateParams.id;
            ctrl.currentUserService = currentUserService;
            ctrl.photoService = photoService;
            ctrl.isSelectPhoto = false;
            ctrl.photos = [];
            ctrl.paymentService = paymentService;

            ctrl.firstPhotosArray = [];
            ctrl.secondPhotosArray = [];
            ctrl.thirdPhotosArray = [];

            if (currentUserService.isAuthenticated()) {
                ctrl.photoIdnex = "originalPhoto";
                ctrl.getPhotoSet(ctrl.photoSetId); 
                
            }
            else {
                ctrl.photoIdnex = "phobroPhoto";
                ctrl.getPhotoSetDateCreate(ctrl.photoSetId);
            }


        };
       
        ctrl.selectPhotos = function (event, photoId) {
            if (ctrl.paymentService.selectedPhotos.indexOf(photoId) == -1) {
                event.stopPropagation();
                ctrl.paymentService.selectedPhotos.push(photoId);
                ctrl.paymentService.isSelectPhoto = true;
            }
            else {
                ctrl.paymentService.selectedPhotos.splice(ctrl.paymentService.selectedPhotos.indexOf(photoId), 1);
                event.stopPropagation();
            }
        };

        ctrl.selectPhoto = function (files, invalidFile) {
            $timeout(function () {
                if ((invalidFile && invalidFile.length && invalidFile[0].$error && invalidFile[0].$error == "maxFiles")  || (ctrl.photos.length + files.length) > 10) {
                    var messege = "You can upload maximum 10 photos in one photoset.";

                    var modalInstance = $uibModal.open({
                        animation: true,
                        component: 'errorModal',
                        windowClass: 'share-photo-modal',
                        resolve: {
                            messege: function () {
                                return messege;
                            }
                        }
                    }).result.then(function () { }, function (res) { });
                }
                else
                {
                    for (var i = 0; i < files.length; i++) {
                        if (!files[i]["$ngfWidth"] || !files[i]["$ngfHeight"]) {
                            return;
                        }
                    }

                    if (files && invalidFile.length == 0) {
                        ctrl.filesUpload = files;
                        ctrl.isUpload = true;
                        ctrl.showError = false;
                        ctrl.progress = true;
                        ctrl.serverError = '';

                        photoService.uploadPhotos(files, ctrl.photoSetId).then(onSuccess, onError);

                        function onSuccess(response) {
                            if (!response) {
                                ctrl.isUpload = false;
                                ctrl.progress = false;
                                return;
                            }
                            ctrl.progress = false;
                            ctrl.isUpload = false;
                            ctrl.photoset.isShared = false;

                            ctrl.photos.push.apply(ctrl.photos, response);
                            ctrl.firstPhotosArray = [];
                            ctrl.secondPhotosArray = [];
                            ctrl.thirdPhotosArray = [];

                            ctrl.photoService.setPhotosToArrays(ctrl.photos, ctrl.firstPhotosArray, ctrl.secondPhotosArray, ctrl.thirdPhotosArray);
                            skip = skip + response.length;

                            if (response && response.data && response.data.error) {
                                ctrl.serverError = response.data.error;
                            } else if (response && response.data && response.data.path) {
                                ctrl.photo = response.data.path;
                            }
                        }

                        function onError(response) {
                            ctrl.progress = false;
                            ctrl.isUpload = false;
                            var data = response.data;

                            var messege = data.error.description;
                            var modalInstance = $uibModal.open({
                                animation: true,
                                component: 'errorModal',
                                windowClass: 'share-photo-modal',
                                resolve: {
                                    messege: function () {
                                        return messege;
                                    }
                                }
                            }).result.then(function () { }, function (res) { });

                        }

                    } else if (invalidFile && invalidFile.length) {
                        var messege = "One or more photos are incorrect. Photo must be at least 1000x1000 pixels and match .jpg, .jpeg, .png formats.";
                            var modalInstance = $uibModal.open({
                                animation: true,
                                component: 'errorModal',
                                windowClass: 'share-photo-modal',
                                resolve: {
                                    messege: function () {
                                        return messege;
                                    }
                                }
                            }).result.then(function () { }, function (res) { });
                        ctrl.showError = true;
                      }
                }
            }, 500);
        };

        ctrl.deletePhoto = function (photoId, event) {
            event.stopPropagation();
            if (!ctrl.progress) {
                ctrl.progress = true;

                var isAccept = false;
                var messege = "Are you sure you want to delete this photo?";
                var modalInstance = $uibModal.open({
                    animation: true,
                    component: 'acceptModal',
                    windowClass: 'accept-photo-modal',
                    resolve: {
                        isAccept: function () {
                            return isAccept;
                        },
                        messege: function () {
                            return messege;
                        }
                    }
                }).result.then(function (isAccept) {
                    if (isAccept) {
                        photoService.deletePhoto(photoId, ctrl.photoSetId).then(onSuccess, onError);
                    }
                    else {
                        ctrl.progress = false;
                    }
                    }, function (res) { });
        
            }

            function onSuccess(data) {
                ctrl.progress = false;
                angular.forEach(ctrl.photos, function (photo) {
                    if (photo.id == photoId)
                    {
                        ctrl.photos.splice(ctrl.photos.indexOf(photo), 1);
                        skip = skip - 1;
                    }                    
                })              

                ctrl.firstPhotosArray = [];
                ctrl.secondPhotosArray = [];
                ctrl.thirdPhotosArray = [];
                ctrl.photoService.setPhotosToArrays(ctrl.photos, ctrl.firstPhotosArray, ctrl.secondPhotosArray, ctrl.thirdPhotosArray);
                delete ctrl.serverError;
            };

            function onError(response) {
                ctrl.progress = false;
                var data = response.data;
                if (data && data.error)
                    ctrl.serverError = data.error.description;
            };
        };

        ctrl.getMorePhotos = function () {
            if (!ctrl.progress && !ctrl.noMore) {
                ctrl.progress = true;

                if (currentUserService.id) {
                    photoService.getPhotos(skip, ctrl.photoSetId).then(onSuccess, onError);

                    function onSuccess(response) {
                        skip = skip + response.length;
                        ctrl.progress = false;

                        if (response && response.length === 0) {
                            ctrl.noMore = true;
                        }

                        ctrl.photos.push.apply(ctrl.photos, response);
                        ctrl.photoService.setPhotosToArrays(response, ctrl.firstPhotosArray, ctrl.secondPhotosArray, ctrl.thirdPhotosArray);

                        delete ctrl.serverError;
                    };

                    function onError(response) {
                        ctrl.progress = false;
                        var data = response.data;
                        if (data && data.error)
                            ctrl.serverError = data.error.description;
                    };
                }
                else {
                    photoService.getClientPhotos(skip, ctrl.photoSetId).then(onSuccess, onError);

                    function onSuccess(response) {

                        skip = skip + response.length;
                        ctrl.progress = false;

                        if (response && response.length === 0) {
                            ctrl.noMore = true;
                        }

                        ctrl.photos.push.apply(ctrl.photos, response);
                        ctrl.photoService.setPhotosToArrays(response, ctrl.firstPhotosArray, ctrl.secondPhotosArray, ctrl.thirdPhotosArray);
                        delete ctrl.serverError;
                    };

                    function onError(response) {
                        ctrl.progress = false;
                        var data = response.data;
                        if (data && data.error)
                            ctrl.serverError = data.error.description;
                    };
                }
            }
        };    

        ctrl.getPhotoSet = function (photoSetId) {

            photoSetsService.getPhotoSet(photoSetId).then(onSuccess, onError)

            function onSuccess(response) {
                ctrl.progress = false;
                ctrl.photoset = response;
                delete ctrl.serverError;

                ctrl.deleteTime = addDays(ctrl.photoset.date, 30);
            };

            function onError(response) {
                ctrl.progress = false;
                var data = response.data;
                if (data && data.error)
                    ctrl.serverError = data.error.description;
            };
        };

        ctrl.getPhotoSetDateCreate = function (photoSetId) {

            photoSetsService.getPhotoSetDateCreate(photoSetId).then(onSuccess, onError)

            function onSuccess(response) {
                ctrl.progress = false;
                ctrl.photoset = response;
                delete ctrl.serverError;

                ctrl.deleteTime = addDays(ctrl.photoset.date, 30);
            };

            function onError(response) {
                ctrl.progress = false;
                var data = response.data;
                if (data && data.error)
                    ctrl.serverError = data.error.description;
            };
        };

        ctrl.downloadPhotos = function () {
            return paymentService.downloadPhotos().then(onSuccess, onError);

            function onSuccess(data) {

                ctrl.progress = false;

                var blob = new File([data], { type: 'application/zip' });
                FileSaver.saveAs(blob, "photos.zip");
            };

            function onError(response) {
                ctrl.progress = false;
                var data = response.data;
                if (data && data.error)
                    ctrl.serverError = data.error.description;
            };
        };

        ctrl.checkout = function () {
            paymentService.checkout(ctrl.paymentService.selectedPhotos);
            $state.go('payment', { id: ctrl.photoSetId });
        };

        function addDays(startDate, numberOfDays) {
            var dateNow = new Date();
            var returnDate = new Date(startDate);
            returnDate = new Date(returnDate.setDate(returnDate.getDate() + 30 - dateNow.getDate()));
           
            return returnDate;
        }

        ctrl.sharePhotos = function () {

            if (!ctrl.progress && ctrl.photos.length !=0) {

                ctrl.progress = true;

                photoSetsService.sharePhotos(ctrl.photoSetId).then(onSuccess, onError);
            }
            else {
                var messege = "Photoset must have at least one photo.";
                var modalInstance = $uibModal.open({
                    animation: true,
                    component: 'errorModal',
                    windowClass: 'share-photo-modal',
                    resolve: {
                        messege: function () {
                            return messege;
                        }
                    }
                }).result.then(function () { }, function (res) { });
            }

            function onSuccess(data) {
                ctrl.progress = false;

                var messege = "You have successfully shared photos with ";

                var modalInstance = $uibModal.open({
                    animation: true,
                    component: 'successModal',
                    windowClass: 'share-photo-modal',
                    resolve: {
                        messege: function () {
                            return messege + ctrl.photoset.clientEmail;
                        }
                    }
                })
                    .result.then(function () { }, function (res) { });

                ctrl.photoset.isShared = true;

                delete ctrl.serverError;
            };

            function onError(response) {
                ctrl.progress = false;
                var data = response.data;
                if (data && data.error)
                    ctrl.serverError = data.error.description;
            };
        };
    }

    angular.module('app')
        .component('photoSetPage', {
            templateUrl: '/app/photoSet/photoSet.html',
            controller: photoSetController
        })
})();
