﻿(function () {
    angular.module('ss.termsOfUse', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('termsOfUse', {
                    url: "/termsofuse",
                    template: '<terms-of-use-page></terms-of-use-page>',
                    data: {
                        pageTitle: 'Terms of Use',
                        noLogin: true
                    }
                })
        }])
})();