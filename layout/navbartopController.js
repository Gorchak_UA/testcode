﻿(function () {
    'use strict';

    function navbartopController($state, accountService, currentUserService, appConfig, photoSetsService, $uibModal, utilService, $rootScope, profileService,$stateParams) {
        var ctrl = this;
        ctrl.currentUserService = currentUserService;

        ctrl.appConfig = appConfig;
        ctrl.state = $state;
        ctrl.profileService = profileService;
        ctrl.progress = false;
        ctrl.stateId = $stateParams.id;

        ctrl.profilePopover = {
            templateUrl: '/app/layout/navbartopPopover.html',
        };

        ctrl.utilService = utilService;

        ctrl.showNavbartop = true;
        ctrl.alternativeNavbartop = false;

        $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
            switch (toState.name) {
                case 'home':
                    //ctrl.alternativeNavbartop = true;
                    break;
                case 'signin':
                    ctrl.showNavbartop = false;
                    break;
                default:
                    ctrl.showNavbartop = true;
                    ctrl.alternativeNavbartop = false;
            }
        });

        ctrl.logOut = function () {
            accountService.logOut();
        };

        ctrl.$onInit = function () {

            if (currentUserService.isAuthenticated()) {
                profileService.getShortCurrentUser().then(function (response) {
                    profileService.currentUser = response;
                });
            }
            else {
                if ($stateParams.id) {
                    profileService.getPhotoSetInfo($stateParams.id).then(function (response) {
                        profileService.currentUser = response.fullName;
                        ctrl.clientEmail = response.clientEmail;
                    });
                }
            }  
        };

        ctrl.withdrawFunds = function () {

            if (!ctrl.progress) {
                if (profileService.currentUser.funds != 0) {
                    ctrl.progress = true;

                    profileService.withdrawFunds().then(onSuccess, onError);
                }
                else
                {
                    var messege = "Your account balance: 0";
                    var modalInstance = $uibModal.open({
                        animation: true,
                        component: 'errorModal',
                        windowClass: 'share-photo-modal',
                        resolve: {
                            messege: function () {
                                return messege;
                            }
                        }
                    }).result.then(function () { }, function (res) { });
                }

                function onSuccess(data) {
                    ctrl.progress = false;

                    var messege = "You have successfully withdraw funds";

                    var modalInstance = $uibModal.open({
                        animation: true,
                        component: 'successModal',
                        windowClass: 'share-photo-modal',
                        resolve: {
                            messege: function () {
                                return messege;
                            }
                        }
                    })
                    .result.then(function () { }, function (res) { });

                    delete ctrl.serverError;
                };

                function onError(response) {
                    var data = response.data;
                    if (data && data.error)
                        var messege = "Your funds were already withdrawn";
                    var modalInstance = $uibModal.open({
                        animation: true,
                        component: 'errorModal',
                        windowClass: 'share-photo-modal',
                        resolve: {
                            messege: function () {
                                return messege;
                            }
                        }
                    }).result.then(function () { }, function (res) { });
                    ctrl.progress = false;
                };
            }      
        };

        ctrl.adminPopover = {
            templateUrl: '/app/admin/adminPopover.html',
            isOpen: false
        }
      
    }

    navbartopController.$inject = ['$state', 'accountService', 'currentUserService', 'appConfig', 'photoSetsService', '$uibModal', 'utilService', '$rootScope', 'profileService', '$stateParams'];

    angular.module('app')
        .component('navBarTop', {
            templateUrl: '/app/layout/navbartop.html',
            controller: navbartopController

        });
})();
