﻿(function () {
    'use strict';

    function resetPasswordController($state, $stateParams, accountService, errorsService, validationService) {
        var ctrl = this;

        ctrl.resetPasswordModel = {
            email: $stateParams.email,
            code: $stateParams.code
        };

        ctrl.errors = errorsService;
        ctrl.validation = validationService;
        ctrl.progress = false;

        ctrl.resetPassword = function (form) {
            if (form.$valid && (ctrl.resetPasswordModel.confirmedPassword == ctrl.resetPasswordModel.password && !ctrl.isSpace(ctrl.resetPasswordModel.confirmedPassword))) {

                if (!ctrl.progress) {

                    ctrl.progress = true;

                    accountService.resetPassword(ctrl.resetPasswordModel).then(onSuccess, onError);

                    function onSuccess(data) {
                        $state.go('signin');
                        ctrl.progress = false;
                    }

                    function onError(response) {
                        var data = response.data;
                        if (data && data.error)
                            ctrl.serverError = data.error.description;
                        ctrl.progress = false;
                    }

                }               
            }          
        }

        ctrl.isMatch = function () {
            if (ctrl.resetPasswordModel) {
                if (ctrl.resetPasswordModel.confirmedPassword != ctrl.resetPasswordModel.password) {
                    return true;
                }
                else {
                    return false;
                }
            }
        };

        ctrl.isSpace = function (password) {
            if ((ctrl.resetPasswordModel.confirmedPassword || ctrl.resetPasswordModel.password) && password) {
                var space = "";
                space = angular.copy(password);
                space = space.replace(/\s/g, "");

                if (space.length == 0) {
                    return true;
                }
                else {
                    return false;
                }
            }
            return false;
        };

    }

    resetPasswordController.$inject = ['$state', '$stateParams', 'accountService', 'errorsService', 'validationService'];

    angular.module('app')
        .component('resetPasswordPage', {
            templateUrl: '/app/account/views/resetPassword.html',
            controller: resetPasswordController
        });
})()
