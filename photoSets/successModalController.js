﻿(function () {
    'use strict';

    successModalController.$inject = ['$state', 'accountService', '$timeout'];

    function successModalController($state, accountService, $timeout) {
        var ctrl = this;

        ctrl.$onInit = function () {
            ctrl.messege = ctrl.resolve.messege;
            $timeout(function () {
                ctrl.close();
            },5000);          
        }
     
    }

    angular.module('app')
        .component('successModal', {
            templateUrl: '/app/photoSets/views/successModal.html',
            controller: successModalController,
            bindings: {
                resolve: '=',
                close: '&',
                dismiss: '&'
            }
        })
})();
