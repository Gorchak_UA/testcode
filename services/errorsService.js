﻿(function () {
    'use strict';

    function errorsService($http, validationService) {

        var error = this;

        error.email = {
            invalid: 'Incorrect email. Please check and try again.',
            required: 'This field is required.'
        };

        error.fullName = {
            required: 'This field is required.',
            maxLength: 'Full name must be no more than ' + validationService.fullName.maxLength + ' characters long.',
            minLength: 'Full name must be no less than ' + validationService.fullName.minLength + ' characters long.',
            invalid: 'Incorrect full name. Please, check and try again.'
        };

        error.payPalId = {
            invalid: 'Incorrect PayPal Id. Please check and try again.',
            required: 'This field is required.'
        };

        error.password = {
            incorrect: 'Passwords do not match.',
            required: 'This field is required.',
            minLength: 'Password must be no less than ' + validationService.password.minLength + ' characters long.',
            maxLength: 'Password must be no more than ' + validationService.password.maxLength + ' characters long.'
        };

        error.confirmPassword = {
            match: 'Your new password doesn\'t match your confirmation password'
        };

        error.messege = {
            required: 'This field is required.',
            minLength: 'Your message must be no less than ' + validationService.messege.minLength + ' characters.',
            maxLength: 'Your message must be no more than ' + validationService.messege.maxLength + ' characters.'
        };

        error.incorrectEmailPassword = 'This email address is already registered. Please login.';
    };

    errorsService.$inject = ['$http', 'validationService'];

    angular
      .module('errors.service', [])
      .service('errorsService', errorsService);

})();