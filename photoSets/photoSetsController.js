﻿(function () {
    'use strict';

    photoSetsController.$inject = ['$state', 'accountService', '$uibModal', 'errorsService', 'validationService', 'photoSetsService'];

    function photoSetsController($state, accountService, $uibModal, errorsService, validationService, photoSetsService) {
        var ctrl = this;

        ctrl.isClickedAddPhotoSet = false;
        ctrl.errors = errorsService;
        ctrl.validation = validationService;
        ctrl.progress = false;
        ctrl.photoSets = [];
        ctrl.photoModel = [];  

        ctrl.openCreatPhotoset = function () {
            if (!ctrl.isClickedAddPhotoSet) {
                ctrl.isClickedAddPhotoSet = true;
                return;               
            }
            ctrl.isClickedAddPhotoSet = false;
        };

        ctrl.createPhotoset = function (form) {
            if (form.$valid) {
                if (!ctrl.progress) {

                    ctrl.progress = true;

                    photoSetsService.createPhotoset(ctrl.createPhotosetModel)
                        .then(onSuccess, onError);
                }

                function onSuccess(data) {
                    ctrl.progress = false;
                    ctrl.isClickedAddPhotoSet = false;
                    ctrl.createPhotosetModel = {};
                    ctrl.photoSets.unshift(data);
                    delete ctrl.serverError;
                };

                function onError(response) {
                    ctrl.progress = false;
                    var data = response.data;
                    if (data && data.error)
                        ctrl.serverError = data.error.description;
                };
            }
        };

        ctrl.deletePhotoSet = function (photoSetId, index) {

            if (!ctrl.progress) {

                ctrl.progress = true;

                photoSetsService.deletePhotoSet(photoSetId).then(onSuccess, onError);
            }


            function onSuccess(data) {
                ctrl.progress = false;
                ctrl.photoSets.splice(index, 1);
                delete ctrl.serverError;
            };

            function onError(response) {
                ctrl.progress = false;
                var data = response.data;
                if (data && data.error)
                    ctrl.serverError = data.error.description;
            };
        };

        ctrl.getMorePhotoSets = function () {

            if (!ctrl.progress && !ctrl.noMore && ctrl.photoSets.length >= 10) {
                ctrl.progress = true;
                photoSetsService.getPhotoSets(ctrl.photoSets.length).then(function (response) {
                    ctrl.progress = false;

                    if (response && response.length === 0) {
                        ctrl.noMore = true;                        
                    }                   

                    ctrl.photoSets.push.apply(ctrl.photoSets, response);              
                });
            }
        };

        ctrl.photoSetClick = function (photoSetId, clientEmail, cover, event, index) {

            ctrl.sharePhotos(photoSetId, clientEmail, cover, index);
            event.stopPropagation();
        };

        ctrl.sharePhotos = function (photoSetId, clientemail, cover, index) {

            if (!ctrl.progress && cover) {

                ctrl.progress = true;

                photoSetsService.sharePhotos(photoSetId).then(onSuccess, onError);
            }
            else
            {
                var messege = "Photoset must have at least one photo.";
                var modalInstance = $uibModal.open({
                    animation: true,
                    component: 'errorModal',
                    windowClass: 'share-photo-modal',
                    resolve: {
                        messege: function () {
                            return messege;
                        }
                    }
                }).result.then(function () { }, function (res) { });
            }

            function onSuccess(data) {
                ctrl.progress = false;

                var messege = "You have successfully shared photos with ";

                var modalInstance = $uibModal.open({
                    animation: true,
                    component: 'successModal',
                    windowClass: 'share-photo-modal',
                    resolve: {
                        messege: function () {
                            return messege + clientemail;
                        }
                    }
                })
                .result.then(function () { }, function (res) { });

                ctrl.photoSets[index].isShared = true;

                delete ctrl.serverError;
            };

            function onError(response) {
                ctrl.progress = false;
                var data = response.data;
                if (data && data.error)
                    ctrl.serverError = data.error.description;
            };
        };

        //ctrl.openSuccessPopup = function () {

        //    var modalInstance = $uibModal.open({
        //        animation: true,
        //        component: 'successModal'
        //    });
        //};

    }

    angular.module('app')
        .component('photoSetsPage', {
            templateUrl: '/app/photoSets/photoSets.html',
            controller: photoSetsController,
            bindings: {
                photoSets: '<'
            }
        })
})();
