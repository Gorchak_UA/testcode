﻿(function () {
    'use strict';

    function adminService($http, utilService) {
        var factory = this;
        var api = "/api/v1";

    };

    adminService.$inject = ['$http', 'utilService'];

    angular
        .module('admin.service', [])
        .service('adminService', adminService);

})();