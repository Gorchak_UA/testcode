﻿(function () {
    angular.module('ss.admin', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('admin', {
                    url: "/admin",
                    template: '<admin-page searchusers="$resolve.searchusers"></admin-page>',
                    data: {
                        pageTitle: 'Users'
                    },
                    resolve: {
                        searchusers: ['adminPaymentService', 'currentUserService', '$state', function (adminPaymentService, currentUserService, $state) {
                            if (currentUserService.isAdmin()) {
                                return adminPaymentService.getUsers(0, '');
                            }
                            else {
                                $state.go('home');
                            }                            
                        }]
                    }
                })
                .state('signinadmin', {
                    url: "/admin/signin",
                    template: '<signin-admin-page></signin-admin-page>',
                    data: {
                        pageTitle: 'Sign In Admin',
                        noLogin: true
                    },
                    resolve: {
                        authorize: ['authorizationService', function (authorizationService) {
                            return authorizationService.authorize();
                        }]
                    }
                })
        }])
})();