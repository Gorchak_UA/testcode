﻿(function () {
    'use strict';

    angular
        .module('profile.service', [])
        .service('profileService', profileService);

    profileService.$inject = ['$http', 'utilService'];

    function profileService($http, utilService) {
        var factory = this;
        var api = "/api/v1";

        factory.currentUser = null;

        factory.getShortCurrentUser = function () {
            return $http.get('/api/v1/profile/getShortCurrentUser')
                .then(function (response) {
                    
                    factory.currentUser = response.data;
                    return response.data;
                });
        };

        factory.getPhotoSetInfo = function (photoSetId) {
            return $http.get('/api/v1/profile/getPhotoSetInfo', { params: { photoSetId: photoSetId}})
                .then(function (response) {

                    factory.currentUser = response.data.fullName;
                    return response.data;
                });
        };

        factory.withdrawFunds = function () {
            return $http.post('/api/v1/profile/withdrawFunds');
        };
    };
})();