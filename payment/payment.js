﻿(function () {
    angular.module('ss.payment', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('payment', {
                    url: "/payment/:id",
                    template: '<payment-page isphoto=$resolve.isphoto></payment-page>',
                    data: {
                        pageTitle: 'Payment',
                        noLogin: true
                    },
                    resolve: {
                        isphoto: ['paymentService', '$state', '$stateParams', '$timeout', function (paymentService, $state, $stateParams, $timeout) {
                            $timeout(function () {
                                if (paymentService.selectedPhotos.length == 0)
                                    $state.go('photoset', { id: $stateParams.id });
                            });
                        }]
                    }
                })
        }])
})();