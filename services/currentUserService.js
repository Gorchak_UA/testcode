﻿(function () {
    'use strict';

    angular
        .module('currentUser.service', [])
        .factory('currentUserService', currentUserService);

    currentUserService.$inject = ['$http', '$timeout', 'localStorageService'];

    function currentUserService($http, $timeout, localStorageService) {
        var user = {
            token: null,
            id: null,
            userName: null,
            roles: [],
            expires: null
        };

        user.isAuthenticated = function () {
            if (user.token && new Date(user.expires) > new Date())
                return true;
            else
                return false;
        };

        user.isInRole = function (role) {
            if (!user.roles)
                return false;

            return user.roles.some(function (item) {
                return role.toLowerCase() === item.toLowerCase();
            });
        };

        user.isAdmin = function () {
            if (!user.roles)
                return false;

            if (user.roles.indexOf("Admin") != -1) {
                return true;
            }

            return false;
        };

        return user;
    }
})();



