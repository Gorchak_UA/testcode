﻿(function () {
    angular.module('ss.photobought', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('photobought', {
                    url: "/photobought/:id",
                    template: '<photo-bought-page></photo-bought-page>',
                    data: {
                        pageTitle: 'Photo Bought',
                        noLogin: true
                    }
                })
        }])
})();