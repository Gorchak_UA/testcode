﻿(function () {
    'use strict';

    function escapeAutoFocus($state) {
        
        return {
            link: function (scope, element, attr, ctrl) {
                angular.element('body').bind('keydown', function (event) {
                    if (event.keyCode === 27) {
                        var inputs = angular.element('input');
                        if (inputs.length) {
                            for (var i = 0; i < inputs.length; i++){
                                inputs[i].blur();
                            }
                        }
                    }
                   
                    
                });
            }
        }
    };  

    angular.module('app')
        .directive("escapeAutoFocus", escapeAutoFocus)
})();