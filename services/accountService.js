﻿(function () {
    'use strict';

    angular
        .module('account.service', [])
        .service('accountService', accountService);

    accountService.$inject = ['$http', '$state', 'localStorageService', 'toastr', 'currentUserService', 'utilService'];

    function accountService($http, $state, localStorageService, toastr, currentUserService, utilService) {
        this.signUp = function (model) {
            return $http.post('/api/v1/account/register', model);                
        };

        this.forgotPassword = function (model) {
            return $http.post('/api/v1/account/ForgotPassword', model);               
        };

        this.resetPassword = function (model) {
            return $http.post('/api/v1/account/ResetPassword', model);
        };

        this.changePassword = function (model) {
            return $http.post('/api/v1/settings/ChangePassword', model);
        };

        this.signIn = function (model) {
            return $http.post('/api/token', $.param(model))
                .then(function onSuccess(response) {
                    var currentUser = response.data;

                    if (!currentUser)
                        return;

                    if (currentUser.roles)
                        currentUser.roles = currentUser.roles.split(",");

                    if (currentUser['.expires'])
                        currentUser.expires = new Date(currentUser['.expires']);

                    currentUser.token = currentUser.token_type + ' ' + currentUser.access_token;

                    utilService.extendLeft(currentUserService, currentUser);

                    localStorageService.set('currentUser', currentUser);
                    $http.defaults.headers.common.Authorization = currentUser.token;

                    return response;
                });
        };

        this.logOut = function (model) {
            return $http.post('/api/v1/account/Logout', model)
                .then(function onSuccess(response) {
                    localStorageService.remove('currentUser');
                    delete $http.defaults.headers.common.Authorization;
                    currentUserService.token = null;
                    $state.go('home', {}, { reload: true });
                    return response;
                }).catch(utilService.errorCallbackToastr());
        };

        this.contactUs = function (model) {
            return $http.post('/api/v1/account/contactUs', model);
        };
    }

})();