﻿(function () {
    'use strict';

    acceptModalController.$inject = ['$state', 'accountService', '$timeout'];

    function acceptModalController($state, accountService, $timeout) {
        var ctrl = this;

        ctrl.accept = function (isAccept) {         
            ctrl.close({$value: isAccept});
        }

        ctrl.$onInit = function () {
            ctrl.messege = ctrl.resolve.messege;
        }

    }

    angular.module('app')
        .component('acceptModal', {
            templateUrl: '/app/photoSets/views/acceptModal.html',
            controller: acceptModalController,
            bindings: {
                resolve: '=',
                close: '&',
                dismiss: '&'
            }
        })
})();
