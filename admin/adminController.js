﻿(function () {
    'use strict';

    angular
        .module('ss.admin')
        .component('adminPage', {
            templateUrl: '/app/admin/admin.html',
            controller: adminController,
            bindings: {
                searchusers: '<'
            }
        })

    adminController.$inject = ['adminPaymentService', '$uibModal'];

    function adminController(adminPaymentService, $uibModal) {
        var ctrl = this;
        ctrl.adminPaymentServcie = adminPaymentService;
        ctrl.countPage = 5; 
        ctrl.currentPage = 1;
        ctrl.maxSize = 5;
        ctrl.email = "";
        ctrl.progress = false;

        ctrl.$onInit = function () {
            ctrl.totalItems = ctrl.searchusers.count;
        }

        ctrl.setPage = function (pageNo) {
            ctrl.currentPage = pageNo;
        };

        ctrl.pageChanged = function () {
            adminPaymentService.getUsers((ctrl.currentPage * ctrl.countPage) - ctrl.countPage, ctrl.email)
                .then(function (response) {
                    ctrl.searchusers.users = response.users;
                    ctrl.totalItems = response.count;
                })
        };

        ctrl.sendFunds = function (id, funds) {

            if (!ctrl.progress) {
                if (funds != 0) {
                    ctrl.progress = true;

                    adminPaymentService.sendFunds(id).then(onSuccess, onError);
                }
                else {
                    var messege = "Funds is 0.";
                    var modalInstance = $uibModal.open({
                        animation: true,
                        component: 'errorModal',
                        windowClass: 'share-photo-modal',
                        resolve: {
                            messege: function () {
                                return messege;
                            }
                        }
                    }).result.then(function () { }, function (res) { });
                }

                function onSuccess(data) {

                    angular.forEach(ctrl.searchusers.users, function (item) {
                        if (item.id == id)
                        {
                            item.isWithdraw = false;
                            item.funds = 0;

                            var messege = "You have successfully send funds";

                            var modalInstance = $uibModal.open({
                                animation: true,
                                component: 'successModal',
                                windowClass: 'share-photo-modal',
                                resolve: {
                                    messege: function () {
                                        return messege;
                                    }
                                }
                            })
                                .result.then(function () { }, function (res) { });
                        }
                    });

                    ctrl.progress = false;
                    delete ctrl.serverError;
                };

                function onError(response) {
                    var data = response.data;                   

                    var messege = data.error.description;

                    var modalInstance = $uibModal.open({
                        animation: true,
                        component: 'errorModal',
                        windowClass: 'share-photo-modal',
                        resolve: {
                            messege: function () {
                                return messege;
                            }
                        }
                    }).result.then(function () { }, function (res) { });

                    if (data && data.error)
                        ctrl.serverError = data.error.description;
                    ctrl.progress = false;
                };
            }
            
        };
    }

})();
