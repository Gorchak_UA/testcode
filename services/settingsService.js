﻿(function () {
    'use strict';

    angular
        .module('settings.service', [])
        .service('settingsService', settingsService);

    settingsService.$inject = ['$http', '$state', 'utilService', 'profileService'];

    function settingsService($http, $state, utilService, profileService) {
        var factory = this;
        var api = "/api/v1";

        factory.updateProfile = function (model) {
            return $http.post('/api/v1/settings/UpdateProfile', model)
                .then(function (response) {
                    profileService.currentUser.fullName = model.fullName;                    
                })
                .catch(utilService.errorCallbackToastr());
        };

        factory.getUserSettings = function () {
            return $http.get('/api/v1/settings/getUserSettings')
                .then(utilService.successCallback())
                .catch(utilService.errorCallbackToastr());
        };
    };
})();