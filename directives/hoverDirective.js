﻿(function () {
    'use strict';

    function hoverDirective($state) {

        return {
            link: function (scope, element, attr, ctrl) {
                element.bind('mouseenter', function (event) {
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip()
                    })
                });
            }
        }
    };

    angular.module('app')
        .directive("hoverDirective", hoverDirective)
})();