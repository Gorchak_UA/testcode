﻿(function () {
    'use strict';

    angular
        .module('adminPayment.service', [])
        .service('adminPaymentService', adminPaymentService);

    adminPaymentService.$inject = ['$http', 'utilService'];

    function adminPaymentService($http, utilService) {
        var factory = this;
        var api = "/api/v1";

        factory.getUsers = function (skip, email) {
            return $http.get('/api/v1/adminPayment/getUsers', { params: { skip: skip, email: email } })
                .then(utilService.successCallback())
                .catch(utilService.errorCallbackToastr());
        };

        factory.sendFunds = function (userId) {
            return $http.post('/api/v1/adminPayment/sendFunds/' + userId);
        };
    };
})();